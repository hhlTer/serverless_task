package com.serverless;

import java.util.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<Map<String, Object>, ApiGatewayResponse> {

    private static final Logger LOG = LogManager.getLogger(Handler.class);

    private final String DEFAULT_VALUE = "тим хто не з росії";
    private final String NAME_KEY = "name";

    @Override
    public ApiGatewayResponse handleRequest(Map<String, Object> input, Context context) {
        LOG.info("received: {}", input);
        String name = DEFAULT_VALUE;
        Map<String, Object> body = null;
        final String bodyString = String.valueOf(input.get("body"));
        if(bodyString != null && !bodyString.isEmpty()){
            try {
                body = new ObjectMapper().readValue(bodyString, HashMap.class);
                name = (String) body.getOrDefault(NAME_KEY, DEFAULT_VALUE);
            } catch (Exception e) {
                name = DEFAULT_VALUE;
            }
        }

        final String message = "Доброго вечора " + name + ", ми з України.";
        Response responseBody = new Response(message, input);
        return ApiGatewayResponse.builder()
                .setStatusCode(200)
                .setObjectBody(responseBody)
                .setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
                .build();
    }
}
