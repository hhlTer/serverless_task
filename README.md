# serverless_task

The lambda called via the AWS API Gateway. 
The URL: https://f6ohs267w8.execute-api.us-east-1.amazonaws.com/dev/hello

# REST API:

- POST method
- expected body example:
```json
{
    "name": "User Name"
}
```

- curl request example:

```
curl --location --request POST "https://f6ohs267w8.execute-api.us-east-1.amazonaws.com/dev/hello" --data-raw "{\"name\":\"User Name\"}"
```


- response: 
```json
{
    "statusCode": 200,
    "body": "{\"message\":\"Доброго вечора User Name, ми з України.\",\"input\":{...}}",
    "headers": {
        "X-Powered-By": "AWS Lambda & serverless"
    },
    "isBase64Encoded": false
}
```

# if no body present:

- request:

```
curl --location --request POST "https://f6ohs267w8.execute-api.us-east-1.amazonaws.com/dev/hello"
```

- responce:

```json
{
    "statusCode": 200,
    "body": "{\"message\":\"Доброго вечора тим хто не з росії, ми з України.\",\"input\":{...}}",
    "headers": {
        "X-Powered-By": "AWS Lambda & serverless"
    },
    "isBase64Encoded": false
}
```

# To deploy new Gateway:

- register AWS credentials:

`serverless config credentials --provider aws --key aws_programm_key --secret secretkey`

- or with aws cli:

`aws configure`

- from the project folder:

`mvn package`
`serverless deploy`

- use URL from the answer 



